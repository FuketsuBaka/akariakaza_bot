from sqlalchemy import Column, String, Text, ForeignKey, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER, BIGINT
from datetime import datetime

from settings.database_settings import *

Base = declarative_base()


class Log(Base):
    __tablename__ = DB_LOG

    id = Column('id', INTEGER(display_width = 11),
                primary_key = True,
                autoincrement = True)
    bot = Column('bot', String(50),
                 nullable = True, default = None,
                 index = True,
                 comment = 'Bot Name')
    user_id = Column('user_id', BIGINT(display_width = 20),
                     ForeignKey('{base}.{col}'.format(base = DB_USERS, col = 'id')),
                     index = True,
                     comment = 'User ID')
    user_name = Column('user_name', Text,
                       nullable = True, default = None,
                       comment = 'User name')
    chat_id = Column('chat_id', BIGINT(display_width = 20),
                     ForeignKey('{base}.{col}'.format(base = DB_CHATS, col = 'id')),
                     index = True,
                     comment = 'Chat ID', )
    chat_type = Column('chat_type', String(50),
                       nullable = True, default = None,
                       index = True,
                       comment = 'Chat type')
    chat_name = Column('chat_name', Text,
                       nullable = True, default = None,
                       comment = 'Chat name')
    time_stamp = Column('time_stamp', DateTime,
                        default = datetime.now(),
                        index = True,
                        comment = 'Timestamp')
    msg_id = Column('msg_id', BIGINT(display_width = 20),
                    index = True,
                    comment = 'MSG ID', )
    msg_text = Column('message_text', Text,
                      nullable = True, default = None,
                      comment = 'MSG Text')
    msg_info = Column('message_info', Text,
                      nullable = True, default = None,
                      comment = 'MSG Info')

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                value_to_init = dictionary[key]
                setattr(self, key, value_to_init)
        for key in kwargs:
            setattr(self, key, kwargs[key])


class Chats(Base):
    __tablename__ = DB_CHATS
    id = Column('id', BIGINT(display_width = 20),
                primary_key = True,
                comment = 'Chat ID', )
    type = Column('type', String(50),
                  nullable = True, default = None,
                  index = True,
                  comment = 'Chat type')
    name = Column('name', Text,
                  nullable = True, default = None,
                  comment = 'Chat name')
    updated = Column('updated', DateTime(timezone = True),
                     server_default = func.now(),
                     comment = 'last time updated')

    messages = relationship('Log')

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                value_to_init = dictionary[key]
                setattr(self, key, value_to_init)
        for key in kwargs:
            setattr(self, key, kwargs[key])


class Users(Base):
    __tablename__ = DB_USERS
    id = Column('id', BIGINT(display_width = 20),
                primary_key = True,
                comment = 'User ID')
    name = Column('name', Text,
                  nullable = True, default = None,
                  comment = 'User name')
    updated = Column('updated', DateTime(timezone = True),
                     server_default = func.now(),
                     comment = 'last time updated')

    messages = relationship('Log')

    def __init__(self, *initial_data, **kwargs):
        for dictionary in initial_data:
            for key in dictionary:
                value_to_init = dictionary[key]
                setattr(self, key, value_to_init)
        for key in kwargs:
            setattr(self, key, kwargs[key])


MODEL_LOG_STR = {
    'bot': 'Akari',
    'user_id': 0,
    'user_name': None,
    'chat_id': 0,
    'chat_type': None,
    'chat_name': None,
    'time_stamp': None,
    'msg_id': 0,
    'msg_text': None,
    'msg_info': None,
}
MODEL_CHAT_STR = {
    'id': None,
    'type': None,
    'name': None,
}
MODEL_USER_STR = {
    'id': None,
    'name': None,
}
