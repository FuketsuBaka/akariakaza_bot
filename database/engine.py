from sqlalchemy import create_engine
from sqlalchemy import text
from sqlalchemy.orm import sessionmaker

import os

import database.models as SC
from modules.classes import Container
from settings.database_settings import *

USE_ECHO = False
if os.name == 'nt':
    USE_ECHO = True

engine = create_engine('mysql://{username}:{password}@{host}:{port}/{db}?charset=utf8'.format(
    username=SQL_USER,
    password=SQL_PASSWORD,
    host=SQL_HOST,
    port=SQL_PORT,
    db=SQL_DB), encoding='utf-8', echo=USE_ECHO)

Session = sessionmaker(bind=engine)
session = Session()


def merge_data(source, target):
    if not source:
        return target
    for key in source.keys():
        target[key] = source[key]
    return target


def init_connection():
    SC.Base.metadata.create_all(bind=engine)


# --------------- OPERATIONS
def insert_string_log(message):
    container = Container(message)
    data = container.dict_log['vals']
    # Chat model. Update on every insert
    chat_model = SC.MODEL_CHAT_STR.copy()
    chat_model['id'] = data['chat_id']
    chat_model['type'] = data['chat_type']
    chat_model['name'] = data['chat_name']
    new_string = SC.Chats(chat_model)
    session.merge(new_string)

    # User model. Update on every insert
    user_model = SC.MODEL_USER_STR.copy()
    user_model['id'] = data['user_id']
    user_model['name'] = data['user_name']
    new_string = SC.Users(user_model)
    session.merge(new_string)

    # fill base_model with data
    base_model = merge_data(data, SC.MODEL_LOG_STR.copy())
    new_string = SC.Log(base_model)
    session.add(new_string)
    commit_session()


def get_distinct_chats():
    return session.query(SC.Log.chat_id).distinct()


def get_last_user_names(id, limit=0):
    return session.query(
        SC.Log.user_id, SC.Log.user_name).filter(
        SC.Log.user_id == id).distinct(
        SC.Log.user_id, SC.Log.user_name).group_by(
        SC.Log.user_id, SC.Log.user_name).order_by(
        SC.Log.time_stamp.desc())[1:limit]


def get_last_chat_names(id, limit=0):
    return session.query(
        SC.Log.chat_id, SC.Log.chat_name).filter(
        SC.Log.chat_id == id).distinct(
        SC.Log.chat_id, SC.Log.chat_name).group_by(
        SC.Log.chat_id, SC.Log.chat_name).order_by(
        SC.Log.time_stamp.desc())[1:limit]


# ---------------- OTHER
def get_table_content_all(table):
    table_content = session.query(table).all()
    for row in table_content:
        print(row.id)


def set_foreign_checks(value):
    _query = text('SET FOREIGN_KEY_CHECKS = {value}'.format(
        value=value))
    session.execute(_query)


def commit_session():
    try:
        session.commit()
    except Exception as e:
        print(e)
        session.rollback()


def bulk_commmit_session(bulk_stack):
    try:
        session.bulk_save_objects(bulk_stack)
        session.commit()
    except Exception as e:
        print(e)
        session.rollback()


def merge_session():
    try:
        session.merge()
    except Exception as e:
        print(e)
        session.rollback()


def close_session():
    session.close()
