from settings.modules_settings import CATAPI
from modules.utils import is_present, post_content
from modules.messages import MSG_CATAPI
import requests


def catapi(bot, message):
    def _main():
        no_caption = False
        if is_present(['-nc', '-nocap'], message.text):
            no_caption = True
        # sleep(.25)
        _post_cat(no_caption)

    def _post_cat(no_caption):
        token = CATAPI['TOKEN']
        api_categories = CATAPI['CATEGORIES']
        url_base = CATAPI['BASEURL']
        api_imgsize = 'full'

        options = message.text.split()[1:]
        api_type = ''
        api_category = ''

        # -------------
        r = ''
        caption_str = ''

        for x in options:
            if x == 'gif':
                api_type = 'type={0}'.format(x)
            if x in ['jpg', 'png']:
                api_type = 'type={0}'.format(x)
            if x in api_categories:
                api_category = 'category={0}'.format(x)

        format_str = '%(url_base)s?api_key=%(api_key)s?size=%(size)s' % {
            'url_base': url_base,
            'api_key': token,
            'size': api_imgsize}
        if api_type != '':
            format_str = format_str + '&{0}'.format(api_type)
        if api_category != '':
            format_str = format_str + '&{0}'.format(api_category)
        try:
            r = requests.get(format_str, timeout=(10, 2), stream=True)
        except requests.exceptions.ReadTimeout:
            raise Exception(MSG_CATAPI['READ_TIMEOUT'])
        except requests.exceptions.ConnectTimeout:
            raise Exception(MSG_CATAPI['CONN_TIMEOUT'])
        except requests.exceptions.ConnectionError:
            raise Exception(MSG_CATAPI['NO_CONN'])
        r = r.url
        caption_str = r

        post_content(bot, message, 'api', 'url', r, no_caption=no_caption, caption_str=caption_str)

    _main()
