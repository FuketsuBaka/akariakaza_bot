import ast
import random
import re


def roll(bot, message):
    # possible params
    roll_from = 1
    roll_to = 100
    params = message.text.lower().split()
    if len(params) > 1:
        # is this x-y?
        pattern_from_to = re.compile("^[0-9]+\-{1}[0-9]+$")
        pattern_to = re.compile("^[0-9]+$")
        if pattern_from_to.match(params[1]):
            roll_from = ast.literal_eval(params[1].split('-')[0])
            roll_to = ast.literal_eval(params[1].split('-')[-1])
            if roll_from > roll_to:
                roll_to = roll_from
        elif pattern_to.match(params[1]):
            roll_to = ast.literal_eval(params[1])
    rng = random.randint(roll_from, roll_to)
    rng_str = apply_digits_grouping('{0}'.format(rng))
    rng_str = get_emoji_str(rng_str)
    bot.reply_to(message, '{0}'.format(rng_str), parse_mode='HTML')


def dice(bot, message):
    rng1 = random.randint(1, 6)
    rng2 = random.randint(1, 6)
    rng_str = get_emoji_str('{0}✖️{1}'.format(rng1, rng2))
    bot.reply_to(message, '{0}'.format(rng_str), parse_mode='HTML')


def apply_digits_grouping(src_str):
    src_list = [x for x in src_str][::-1]
    result_list = []
    for i in range(1, len(src_list) + 1):
        result_list.append(src_list[i - 1])
        if i % 3 == 0 and i != len(src_list):
            result_list.append('   ')
    result_list = result_list[::-1]
    result_str = ''.join(result_list)
    return result_str


def get_emoji_str(str_to_conv):
    keys = '1,2,3,4,5,6,7,8,9,0'
    vals = '1️⃣,2️⃣,3️⃣,4️⃣,5️⃣,6️⃣,7️⃣,8️⃣,9️⃣,0️⃣'
    mapping = dict(zip(keys.split(','), vals.split(',')))
    table = str.maketrans(mapping)
    return str_to_conv.translate(table)
