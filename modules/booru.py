import requests, urllib, random, re, ast
from store import BOORU_LAST_URL, set_booru_last
from modules.utils import post_content
from modules.messages import MSG_BOORU
from modules.classes import Html
from settings.modules_settings import BOORU


def booru(bot, message):
    def _main():
        _post_pic()

    # ----------------------------------------------
    def _post_pic():
        tags = message.text.lower().split()[1:]
        tag_no_comic = True
        tag_safe = True
        is_repeat = False
        tag_str = '&tags='

        rng_b = random.randint(0, len(BOORU['LIST']['src']) - 1)
        source_booru = BOORU['LIST']['src'][rng_b]
        pid_limit = BOORU['LIST']['pid_limit'][rng_b]
        h1_empty = BOORU['LIST']['h1_empty'][rng_b]
        if len(tags) > 0:
            for x in tags:
                if x == '-r' and BOORU_LAST_URL != '':
                    is_repeat = True
                    break
                elif x == 'comic':
                    tag_no_comic = False
                elif x.find('rating') != -1 or 'rating' in x:
                    tag_safe = False
                    tag_str = '{0}+{1}'.format(tag_str, x)
                else:
                    tag_str = '{0}+{1}'.format(tag_str, x)
        tag_str = urllib.parse.quote(tag_str, safe='/+&?=')
        if tag_no_comic:
            tag_str = '{0}+{1}'.format(tag_str, BOORU['TAG_COMIC'])
        if tag_safe:
            tag_str = '{0}+{1}'.format(tag_str, BOORU['TAG_SAFE'])
        if is_repeat:
            r = BOORU_LAST_URL
        else:
            r = '{source}?{opt_s}&{opt_p}{tags}'.format(
                source=source_booru,
                opt_s=BOORU['S'], opt_p=BOORU['P'],
                tags=tag_str)
        save_r = r
        # ------------------------------------------------------
        pic_url, pic_full_url = _get_random_pic(r, source_booru, pid_limit, h1_empty)
        if pic_url == 'nothing':
            bot.reply_to(message, pic_full_url, parse_mode='HTML')
        else:
            set_booru_last(save_r)
            post_content(bot, message, 'api', 'url', pic_url, no_caption=False, caption_str=pic_full_url)

    # ----------------------------------------------
    def _get_random_pic(r, source_booru, pid_limit, h1_empty):
        def _random_pic_main(r, source_booru, pid_limit, h1_empty):
            # prepare class
            cl = Booru(r, source_booru, pid_limit, h1_empty)
            if not cl.is_valid_page():
                return ['nothing', h1_empty]
            # first call - list of pages
            cl.get_random_page()
            cl.get_random_pic_on_page()

            if cl.img_url != '':
                return [cl.img_url, cl.img_url]
            else:
                return ['nothing', MSG_BOORU['NO_IMG_GOT']]

        return _random_pic_main(r, source_booru, pid_limit, h1_empty)

    _main()


class Booru(Html):
    def __init__(self, url, source_booru, pid_limit, h1_empty):
        self.base_url = url
        self.page_url = url
        self.post_url = ''
        self.img_url = ''
        self.max_pid = 0
        self.page_pid = 0
        self.pid_limit = pid_limit
        self.source_booru = source_booru
        self.H1_EMPTY = h1_empty
        self.set_DOME(url)

    # super(CL_BOORU, self).__init__(url)
    # return BOOLEAN
    def is_valid_page(self):
        items = self.get_items_by_filter('h1')
        for item in items:
            if item.html().find(self.H1_EMPTY) != -1:
                return False
        return True

    def get_pid_random(self):
        self.get_pid_max()
        if self.max_pid > 0:
            self.page_pid = random.randint(1, self.max_pid)

    def get_pid_max(self):
        items = self.get_items_by_filter('.pagination > a')
        if items.len > 0:
            item = items[-1]
            link = self.get_item_attrib(item, 'href')
            l = re.findall(r'pid=(\d+)', link)
            if len(l) > 0:
                self.max_pid = int(l[-1])
            if self.max_pid > self.pid_limit:
                self.max_pid = self.pid_limit
        else:
            # it's just 1 page
            self.max_pid = 0

    def get_random_page(self):
        self.get_pid_random()
        if self.page_pid != 0:
            self.page_url = '{0}&pid={1}'.format(self.base_url, str(self.page_pid))
        else:
            self.page_url = self.base_url
        self.set_DOME(self.page_url)

    def get_random_pic_on_page(self):
        items = self.get_items_by_filter('.thumb > a')
        if items.len > 0:
            rng = random.randint(0, items.len - 1)
            item = items[rng]
            pic_ID = self.get_item_attrib(item, 'id')[1:]
            self.post_url = '{source}?{opt_p}&{opt_v}&id={pic_id}'.format(
                source=self.source_booru,
                opt_p=BOORU['P'], opt_v=BOORU['V'],
                pic_id=pic_ID)
            self.set_DOME(self.post_url)
            self.get_pic_url_on_page()
        elif self.page_url != self.base_url:
            self.set_DOME(self.base_url)
            self.get_random_pic_on_page()
        else:
            # there is no pics on page. Wierd but we should raise smthng. idk.
            return

    def get_pic_url_on_page(self):
        items = self.get_items_by_filter('img[id=image]')
        if items.len > 0:
            item = items[-1]
            self.img_url = self.get_item_attrib(item, 'src')
            self.img_url = re.sub(r'(\?\d+)', '', self.img_url)
            self.img_url = self.img_url.replace('//safebooru.org//', 'http://safebooru.org/')
        else:
            return
