from settings.env_settings import *


def check_blacklisted(user_id, chat_id):
    is_blacklisted = False
    if check_blacklisted_user(user_id):
        # print( 'user with id: {0} - is blacklisted'.format(user_id) )
        is_blacklisted = True
    if check_blacklisted_chat(chat_id):
        # print( 'chat with id: {0} - is blacklisted'.format(chat_id) )
        is_blacklisted = True
    return is_blacklisted


def check_blacklisted_user(user_id):
    user_id = str(user_id)
    bl_users = read_blacklist_file('bl_users.list')
    return user_id in bl_users


def check_blacklisted_chat(chat_id):
    chat_id = str(chat_id)
    bl_chats = read_blacklist_file('bl_chats.list')
    return chat_id in bl_chats


def read_blacklist_file(filename):
    bl_list = []
    try:
        with open("{0}{1}".format(PATH_BASE, filename), 'r') as f:
            lines = f.readlines()
            for line in lines:
                e = line.strip().split()[0]
                # print(e, not e.startswith('#'))
                if e.startswith('#'):
                    continue
                else:
                    bl_list.append(e)
        # bl_list =[e.strip().split()[0] for e in lines]
    except:
        return []
    return bl_list


def get_bl_status_string(user_id='', chat_id=''):
    bl_user = ''
    bl_chat = ''
    if user_id != '' and check_blacklisted_user(user_id):
        bl_user = ' [+b]'
    if chat_id != '' and check_blacklisted_chat(chat_id):
        bl_chat = ' [+b]'
    return [bl_user, bl_chat]
