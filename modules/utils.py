import string, os, sh, shutil, socket, requests, re, humanize, subprocess
from collections import Counter
from datetime import datetime
from operator import itemgetter

from telebot import *
from modules.messages import MSG_SYS, MSG_BTNS
from settings.settings import (
    TELEGRAM_API_URL, TELEGRAM_BASE_URL,
    BOT_TOKEN, MEDIA,
    SU_ID, BOT_ID,
    PATH_TEMP, LIMITS,
)


# Nothing in return. Operate with bot and message.
# method - 'api' or 'json'
# source - 'url' or 'local'
# path - url string or filename string
# if source == 'url' method always will be 'api'
# fn_post_content(message, 'api', 'url', path, no_caption = True, no_reply = True, caption_str = '')
def post_content(bot, message, method,
                 source, path, no_caption=False, no_reply=False, caption_str='',
                 reply_to_message_id=None):
    # -------------------- API
    def _send_api():
        def _send_api_content(send_method, content_to_send):
            bot.send_chat_action(message.chat.id, send_method['m_pre'])
            bot_func = getattr(bot, send_method['m_api'])

            caption = None
            if not no_caption:
                caption = caption_str
            if not no_reply:
                reply_to_message_id = message.message_id
            else:
                reply_to_message_id = None

            bot_func(message.chat.id, content_to_send, reply_to_message_id=reply_to_message_id, caption=caption)

        def _api_main():
            send_method = _get_send_method()
            if source == 'url':
                _send_api_content(send_method, path)
            else:
                media_path = str(path)
                media_path, filename = secure_file(media_path, True)
                # media_path = unicode(path, 'utf-8')
                with open(media_path, 'rb') as content_to_send:
                    _send_api_content(send_method, content_to_send)

        _api_main()

    # -------------------- JSON
    def _send_json():
        def _send_json_content(send_method, media_path, caption=None):
            media_path, filename = secure_file(media_path, True)

            URL = '{0}{1}{2}'.format(TELEGRAM_API_URL, BOT_TOKEN, send_method['m_json'])
            with open(media_path, 'rb') as content_to_send:
                message_data = {'chat_id': message.chat.id}
                if caption:
                    message_data['caption'] = str(caption)
                if reply_to_message_id:
                    message_data['reply_to_message_id'] = reply_to_message_id

                files = {
                    send_method['key_json']: content_to_send
                }
                try:
                    request = requests.post(URL, data=message_data, files=files)
                except Exception as error:
                    print('Send message error')
                    print(str(error))
                    return

                if not request.status_code == 200:
                    str_error = 'Request returned error code: {0}'.format(request.status_code)
                    print('----------------------')
                    print('request returned: ', request.status_code)
                    print('media_path: ', media_path)
                    print('message_data: ', message_data)
                    print('files: ', files)

                    raise Exception(str_error)
                    return

        def _json_main():
            send_method = _get_send_method()
            media_path = str(path)
            # media_path = unicode(path, 'utf-8')
            # reply_to_message_id = message.message_id
            caption = None
            if not no_caption:
                caption = caption_str
            _send_json_content(send_method, media_path, caption=caption)

        _json_main()

    # -------------------- Prepare Send Dict
    def _get_send_method():
        ext = path.rsplit('.', 1)[-1].lower()
        send_method = {'m_pre': 'upload_document',
                       'm_json': '/sendDocument',
                       'key_json': 'document',
                       'm_api': 'send_document'}
        if is_present(MEDIA['TYPES']['IMAGE'], ext):
            send_method = {	'm_pre': 'upload_photo',
                            'm_json': '/sendPhoto',
                            'key_json': 'photo',
                            'm_api': 'send_photo'}
        elif is_present(MEDIA['TYPES']['VIDEO'], ext):
            send_method = {	'm_pre': 'upload_video',
                            'm_json': '/sendVideo',
                            'key_json': 'video',
                            'm_api': 'send_video'}
        elif is_present(MEDIA['TYPES']['AUDIO'], ext):
            send_method = {	'm_pre': 'upload_audio',
                            'm_json': '/sendAudio',
                            'key_json': 'audio',
                            'm_api': 'send_audio'}
        return send_method
    # -------------------- MAIN
    def _main(no_caption, no_reply, caption_str):
        if not no_caption and caption_str == '':
            if source == 'url':
                caption_str = path
            else:
                caption_str = os.path.basename(path)
        # caption is limited to 200 chars by telegram API
        caption_str = '{0}..'.format(caption_str[:198]) if len(caption_str) > 200 else caption_str
        if method == 'api' or source == 'url':
            _send_api()
        else:
            _send_json()
    _main(no_caption, no_reply, caption_str)

def get_time_passed(start_time):
    time_now = datetime.now()
    time_delta = time_now - start_time
    return_dict = {}
    d = time_delta.days
    s = time_delta.seconds
    h = s // 3600
    s = s % 3600
    m = s // 60
    return_dict['fulldays'] = d
    return_dict['fullseconds'] = s
    return_dict['hours'] = s // 3600
    return_dict['minutes'] = s % 3600
    return_dict['seconds'] = s // 60
    return_dict['string_time_delta'] = '{d_val} {d_str}, {h_val} {h_str} {m_and} {m_val} {m_str}'.format(
        d_val=d, h_val=h, m_val=m,
        d_str=MSG_SYS['DAYS'], h_str=MSG_SYS['HOURS'], m_str=MSG_SYS['MINS'],
        m_and = MSG_SYS['AND'] )
    return return_dict


def is_present(list_marks, src_str):
    present = False
    for i in list_marks:
        if src_str.find(i) != -1 or i in src_str:
            present = True
    return present

def sort_dict(d, reverse):
    return sorted(d.items(), key = itemgetter(1), reverse = reverse)

# Returns filename secured for storage
def secure_file(path, rename):
    validchars = string.ascii_letters + string.digits + ' ' + '.' + '_' + '-' + '+'
    path_to, filename = os.path.split(path)
    filename_clean = ''.join(c for c in filename if c in validchars)

    media_path_clean = os.path.join(path_to, filename_clean)
    if rename:
        os.rename(path, media_path_clean)

    return [media_path_clean, filename_clean]


def get_urls_from_str(string, pattern=''):
    list_of_urls = None
    if pattern != '':
        list_of_urls = re.findall(r'({0})'.format(pattern), string, re.IGNORECASE)
    else:
        list_of_urls = re.findall(r'(https?://\S+)', string, re.IGNORECASE)
    return list_of_urls


def get_link_to_youtube(list_of_urls):
    for i in list_of_urls:
        if i.find('youtube.com') != -1 or i.find('youtu.be') != -1:
            return i
    return None

def get_host_by_name(hostname):
    return socket.gethostbyname(hostname)

def prepare_markup_tables(list_btns, check_admin, cancel_btn,
                          operation_name, is_admin = False,
                          width = 2, message_id = None):
    markup = types.InlineKeyboardMarkup(row_width=width)
    kb_dict = []
    if width > 3: width = 3
    for i in list_btns:
        kb_dict.append(i)
    if cancel_btn:
        kb_dict.append(MSG_BTNS['CANCEL'])
    while not (len(kb_dict) % width == 0):
        kb_dict.append(None)
    for i in range(0, len(kb_dict), width):
        x = kb_dict[i]
        y = None
        z = None
        if i+ 1 <= len(kb_dict) and width == 2:
            y = kb_dict[i + 1]
        if i + 2 <= len(kb_dict) and width == 3:
            z = kb_dict[i + 2]
        item_btn1 = types.InlineKeyboardButton(
            text=x.split('_')[-1],
            callback_data='{0}|{1}|{2}'.format(
                operation_name, x, message_id))
        if y != None and z != None:
            item_btn2 = types.InlineKeyboardButton(
                text=y.split('_')[-1],
                callback_data='{0}|{1}|{2}'.format(
                    operation_name, y, message_id))
            item_btn3 = types.InlineKeyboardButton(
                text=z.split('_')[-1],
                callback_data='{0}|{1}|{2}'.format(
                    operation_name, z, message_id))
            markup.row(item_btn1, item_btn2, item_btn3)
        elif y != None:
            item_btn2 = types.InlineKeyboardButton(
                text=y.split('_')[-1],
                callback_data='{0}|{1}|{2}'.format(
                    operation_name, y, message_id))
            markup.row(item_btn1, item_btn2)
        else:
            markup.row(item_btn1)
    return markup


def is_admin(user):
    return str(user.id) == SU_ID


def is_this_bot(user):
    return str(user.id) == BOT_ID


def clear_temp_folder():
    dls_size = get_size_of_folder(PATH_TEMP, False)
    if dls_size > LIMITS['DOWNLOADS_MAX_SIZE']:
        shutil.rmtree(PATH_TEMP)
        os.mkdir(PATH_TEMP)


def get_size_of_folder(path_to_dir, is_humanize):
    if os.path.exists(path_to_dir):
        dir_size = sh.du(['-s', '-b'], path_to_dir).split()[0]
        if is_humanize:
            return humanize.naturalsize(dir_size, gnu=True)
        else:
            return int(dir_size)
    else:
        return 0


def get_humanize_size(size):
    return humanize.naturalsize(size, gnu=True)


def rename_file(path_old, path_new):
    os.rename(path_old, path_new)


# ##################################### SYSTEM CALLS ##################################### #
# str = fn_syscall('ls -agh', find_str='Active:', prefix='LS output\n', replace='Active: ')
def syscall(call, find_str=None, prefix='', replace=''):
    def _exec_cmd(cmd):
        p = subprocess.Popen(cmd,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
        return iter(p.stdout.readline, b'')

    def _read_output(cmd, find_str=None):
        return_lines = ''
        for line in _exec_cmd(cmd):
            line_dec = line.decode('utf-8').strip()
            if find_str:
                if line_dec.find(find_str) != -1:
                    return_lines += '{0}\n'.format(line_dec)
            elif line_dec != '':
                return_lines += '{0}\n'.format(line_dec)
        return return_lines

    cmd = call.split()
    return '{0}{1}'.format(prefix, _read_output(cmd, find_str=find_str).replace(replace, ''))


def log_top_ip(log_name):
    def _exec_cmd():
        p = subprocess.Popen(cmd,
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT,
                             shell=True)
        # result = p.communicate()
        return iter(p.stdout.readline, b'')

    cmd = "cat /var/log/%s | grep -oE '\\b[0-9]{1,3}(\\.[0-9]{1,3}){3}\\b'" % log_name

    list_ip = []
    for line in _exec_cmd():
        line_dec = line.decode('utf-8').strip()
        list_ip.append(line_dec)
    # get unique
    dict_ip = Counter(list_ip)
    dict_ip = sorted(dict_ip.items(), key=lambda x: x[1], reverse=True)

    i = 0
    return_str = ""
    for item in dict_ip:
        return_str += "<code>%+15s</code>" % item[0]
        return_str += " — %s\n" % item[1]
        i += 1
        if i >= 10:
            break
    return return_str
