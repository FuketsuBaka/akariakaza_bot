import re, ast, requests, shutil
from settings.settings import TELEGRAM_BASE_URL, BOT_TOKEN
from settings.modules_settings import MEDIA, CONVERT
from settings.env_settings import LIMITS, PATH_TEMP
from modules.messages import MSG_ERRORS, MSG_MPY, MSG_BTNS
from modules.utils import (
	get_urls_from_str,
	get_link_to_youtube,
    prepare_markup_tables,
	clear_temp_folder,
	rename_file,
	post_content,
)
from modules.classes import Container, form_fsize, Hash
from modules.decorators import run_async

from moviepy.editor import VideoFileClip
from pytube import YouTube

# -------------------------- EXPORTS

def convert(bot, message):
	if message.reply_to_message is None:
		raise Exception(MSG_ERRORS['NEED_REPLY'])
	msg_text = message.reply_to_message.text
	# ---------------------------- Youtube?
	if msg_text is not None and msg_text != '':
		list_of_urls = get_urls_from_str(msg_text)
		url_str = get_link_to_youtube(list_of_urls)
		if url_str is not None:
			list_of_videos = pytube_get_videos_list(url_str)
			if list_of_videos is not None and len(list_of_videos) != 0:
				markup = prepare_markup_tables(list_of_videos, False, True, 'convert_youtube')
				bot.reply_to(message, '{0}\n<code>{1}</code>'.format(
					MSG_MPY['REPLIES']['KB_SELECT_RESOLUTION'], url_str),
				             reply_markup = markup, parse_mode = 'HTML')
				return
			else:
				raise Exception(MSG_MPY['ERRORS']['KB_NO_VIDS'] % (LIMITS['LOCAL_SIZE_LIMIT'] * 2))
	# ---------------------------- File
	conv_format, start_sec, stop_sec = get_convert_options(message.text)
	if not conv_format in CONVERT['AVAILABLE_OPTIONS']:
		raise Exception(MSG_MPY['ERRORS']['CANT_CONVERT'] % conv_format)
	convert_this(
		bot, message, message.reply_to_message,
		conv_to = conv_format, start_sec = start_sec,
		stop_sec = stop_sec)

def inline_callback(bot, call):
	message = call.message
	call_data = call.data.split('|')
	call_oper = call_data[0]
	call_text = call_data[1]
	call_msg_id = call_data[2]
	user_obj = call.from_user
	try:
		if call_text == MSG_BTNS['CANCEL']:
			bot.delete_message(message.chat.id, message.message_id)
			return
		if call_oper == 'ask_convert':
			if call_text in MSG_BTNS['BTNS_YES']:
				bot.delete_message(message.chat.id, message.message_id)
				convert_this(bot, message, message.reply_to_message, reply_to_message_id = call_msg_id)
			if call_text in MSG_BTNS['BTNS_NO']:
				bot.delete_message(message.chat.id, message.message_id)
				pass
		if call_oper == 'convert_youtube':
			url_str_list = message.text.splitlines()
			url_str = url_str_list[-1].strip()
			pytube_get_video(bot, message, message.reply_to_message, url_str, resolution = call_text.split(' - ')[1])
	# delete in function

	except Exception as error:
		bot.delete_message(message.chat.id, message.message_id)

# -------------------------- CONVERT

@run_async
def convert_this(bot, message, reply_msg, reply_to_message_id=None, conv_to='mp4', start_sec=None, stop_sec=None):
	def _get_first_link(src_string, pattern=''):
		list_of_urls = get_urls_from_str(src_string, pattern = pattern)
		ret_url = ''
		if list_of_urls is not None and len(list_of_urls) > 0:
			ret_url = list_of_urls[0]
		return ret_url

	try:
		chat_id = message.chat.id
		if reply_to_message_id is None:
			reply_to_message_id = reply_msg.message_id
		clear_temp_folder()

		cl_obj = Container(reply_msg)
		mime_type = cl_obj.dict_media['vals']['mime_type']
		file_id = cl_obj.dict_media['vals']['file_id']
		file_name = cl_obj.dict_media['vals']['file_name']

		is_link_content = False
		pattern = ''
		if file_id is None:
			# it might be a link. get it's content
			if reply_msg.text is not None and reply_msg.text != '':
				if re.search(MSG_MPY['REGEXP']['LINK_WEBM'], reply_msg.text, re.IGNORECASE):
					# it is webm-link
					is_link_content = True
					mime_type = MEDIA['FORMATS']['WEBM']['MIME']
					file_name = 'noname_{0}.{1}'.format(reply_msg.message_id, conv_to)
					pattern = MSG_MPY['REGEXP']['LINK_WEBM']
				elif re.search(MSG_MPY['REGEXP']['LINK_MP4'], reply_msg.text, re.IGNORECASE):
					# it is mp4
					is_link_content = True
					mime_type = MEDIA['FORMATS']['MP4']['MIME']
					file_name = 'noname_{0}.{1}'.format(reply_msg.message_id, conv_to)
					pattern = MSG_MPY['REGEXP']['LINK_MP4']
				elif re.search(MSG_MPY['REGEXP']['LINK_MPG'], reply_msg.text, re.IGNORECASE):
					# it is mpg
					is_link_content = True
					mime_type = MEDIA['FORMATS']['MPG']['MIME']
					file_name = 'noname_{0}.{1}'.format(reply_msg.message_id, conv_to)
					pattern = MSG_MPY['REGEXP']['LINK_MPG']
				elif re.search(MSG_MPY['REGEXP']['LINK_MKV'], reply_msg.text, re.IGNORECASE):
					# it is mkv
					is_link_content = True
					mime_type = MEDIA['FORMATS']['MKV']['MIME']
					file_name = 'noname_{0}.{1}'.format(reply_msg.message_id, conv_to)
					pattern = MSG_MPY['REGEXP']['LINK_MKV']
				elif re.search(MSG_MPY['REGEXP']['LINK_AVI'], reply_msg.text, re.IGNORECASE):
					# it is avi
					is_link_content = True
					mime_type = MEDIA['FORMATS']['AVI']['MIME']
					file_name = 'noname_{0}.{1}'.format(reply_msg.message_id, conv_to)
					pattern = MSG_MPY['REGEXP']['LINK_AVI']
				elif re.search(MSG_MPY['REGEXP']['LINK_FLV'], reply_msg.text, re.IGNORECASE):
					# it is flv
					is_link_content = True
					mime_type = MEDIA['FORMATS']['FLV']['MIME']
					file_name = 'noname_{0}.{1}'.format(reply_msg.message_id, conv_to)
					pattern = MSG_MPY['REGEXP']['LINK_FLV']
				elif re.search(MSG_MPY['REGEXP']['LINK_3GP'], reply_msg.text, re.IGNORECASE):
					# it is flv
					is_link_content = True
					mime_type = MEDIA['FORMATS']['3GP']['MIME']
					file_name = 'noname_{0}.{1}'.format(reply_msg.message_id, conv_to)
					pattern = MSG_MPY['REGEXP']['LINK_3GP']

		if file_name is None:
			file_name = cl_obj.dict_media['vals']['title']
		if file_name is None:
			file_name = 'noname_{0}.{1}'.format(reply_msg.message_id, conv_to)

		caption = cl_obj.obj_msg.msg_caption
		if caption is None or caption == '':
			caption = file_name

		del cl_obj

		media_type, render_type = get_render_type(conv_to)

		if convert_type_missmatch(mime_type, render_type):
			raise Exception(MSG_MPY['ERRORS']['ERROR_CONV_TYPE'].format(conv_to, mime_type))

		if (file_id is not None and file_id != '') or is_link_content:
			file_info = ''
			url_of_file = ''
			if is_link_content:
				url_of_file = _get_first_link(reply_msg.text, pattern = pattern)
				if url_of_file == '':
					raise Exception(MSG_MPY['ERRORS']['FAILED_FILE_HOOK'])
			else:
				file_info = bot.get_file(file_id)
				url_of_file = '{0}{1}/{2}'.format(TELEGRAM_BASE_URL, BOT_TOKEN, file_info.file_path)
			save_path, new_path, new_file_name, new_caption = get_save_and_new_path(file_name, conv_to,
			                                                                                i_1 = start_sec,
			                                                                                i_2 = stop_sec,
			                                                                                caption = caption)

			path_to_existed = Hash().find_existed_md5hash(url_of_file)
			got_file = False
			if path_to_existed is not None:
				save_path = path_to_existed
				got_file = True
			else:
				# need download
				r = requests.get(url_of_file, stream = True)
				if r.status_code == 200:
					with open(save_path, 'wb') as f:
						r.raw.decode_content = True
						shutil.copyfileobj(r.raw, f)
					got_file = True
			if got_file:
				bot.send_chat_action(message.chat.id, 'record_video')
				# check if convert needed
				if start_sec is None and stop_sec is None:
					# it's full file request. Let's check mime vs conv_to
					if conv_to == CONVERT['MIME_TYPES_EXT'][mime_type]:
						# it's a match. Just download and send file
						# print('it\'s a match. Just download and send file')
						# we need to rename downloaded file
						rename_file(save_path, new_path)
						post_content(bot, message, 'json', 'local', new_path, no_caption = False, no_reply = False,
						                caption_str = new_caption, reply_to_message_id = reply_to_message_id)
						return

				convert_file(save_path, new_path, render_type, conv_to, start_sec = start_sec,
				                     stop_sec = stop_sec)

				post_content(bot, message, 'json', 'local', new_path, no_caption = False, no_reply = False,
				                caption_str = new_caption, reply_to_message_id = reply_to_message_id)

	except Exception as e:
		bot.send_message(message.chat.id, MSG_ERRORS['EXCEPTION'].format(str(e)), parse_mode = 'HTML')


def convert_file(save_path, new_path, render_type, conv_to, start_sec=None, stop_sec=None):
	#print start_sec, stop_sec, conv_to, render_type
	# convert
	if render_type == 'to_video':
		clip = VideoFileClip(save_path)
		cod = 'libx264'
		if conv_to == 'avi':
			cod = 'png'
		if conv_to == 'webm':
			cod = 'libvpx'

		if start_sec != None and stop_sec != None:
			if clip.duration >= start_sec:
				if clip.duration < stop_sec:
					stop_sec = clip.duration
				clip2 = clip.subclip(start_sec, stop_sec)
				clip2.write_videofile(new_path, preset='medium', codec = cod)
				return
		#, bitrate='384k', audio_bitrate='384k'
		clip.write_videofile(new_path, preset='medium', codec = cod)
	if render_type == 'to_gif':
		#clip = VideoFileClip(save_path).resize(width=200)
		clip = VideoFileClip(save_path, audio=False)
		width = clip.w
		while width > 250:
			width = width / 2
		if width != clip.w:
			clip.resize(width=width)

		fps = clip.fps
		max_fps = 15
		if fps > max_fps:
			fps = max_fps

		if start_sec != None and stop_sec != None:
			if clip.duration >= start_sec:
				if clip.duration < stop_sec:
					stop_sec = clip.duration
				clip2 = clip.subclip(start_sec, stop_sec)
				#clip2.write_gif(new_path, fps=fps, loop=1, fuzz=2, program='ffmpeg')
				clip2.write_gif(new_path, fps=fps, loop=1, fuzz=2, program='ffmpeg')
				return

		clip.write_gif(new_path, fps=fps, loop=1, fuzz=2, program='ffmpeg')
		#clip.write_gif(new_path, loop=1, fuzz=15, opt='OptimizePlus')
	if render_type == 'to_audio':
		clip = VideoFileClip(save_path)
		audio_clip = clip.audio
		if clip.audio is None:
			raise Exception(MSG_MPY['ERRORS']['ERROR_CONV_NO_AUDIO'])

		if start_sec != None and stop_sec != None:
			if clip.duration >= start_sec:
				if clip.duration < stop_sec:
					stop_sec = clip.duration
				audio_clip2 = audio_clip.subclip(start_sec, stop_sec)
				audio_clip2.write_audiofile(new_path, bitrate='384k')
				return

		audio_clip.write_audiofile(new_path, bitrate='384k')


# -------------------------- YOUTUBE
@run_async
def pytube_get_video(bot, message, reply_msg, url_str, resolution='480p'):
	try:
		reply_to_message_id = reply_msg.message_id
		clear_temp_folder()

		yt = YouTube(url_str)
		video = yt.streams.filter(progressive = True, subtype = 'mp4', resolution = resolution).order_by(
			'resolution').desc().first()
		caption_name = video.default_filename
		file_id = yt.video_id

		file_name = '{0}.mp4'.format(file_id)
		save_path = '{0}'.format(PATH_TEMP)
		save_path_full = '{0}{1}'.format(save_path, file_name)
		bot.delete_message(message.chat.id, message.message_id)
		video.download(save_path, filename = file_id)

		# ----------------- Convert options
		conv_format, start_sec, stop_sec = get_convert_options(reply_msg.text)
		if not conv_format in CONVERT['AVAILABLE_OPTIONS']:
			raise Exception(MSG_MPY['ERRORS']['CANT_CONVERT'] % conv_format)
		if start_sec is None and stop_sec is None and conv_format == 'mp4':
			post_content(bot, message, 'json', 'local', save_path_full, no_caption = False, no_reply = False,
			                caption_str = caption_name, reply_to_message_id = reply_to_message_id)
		else:
			media_type, render_type = get_render_type(conv_format)

			if convert_type_missmatch(MEDIA['FORMATS']['MP4']['MIME'], render_type):
				raise Exception(MSG_MPY['ERRORS']['ERROR_CONV_TYPE'])

			save_path_conv, new_path_conv, new_file_name, new_caption = get_save_and_new_path(
				file_name, conv_format, i_1 = start_sec, i_2 = stop_sec, caption = caption_name)
			bot.send_chat_action(message.chat.id, 'record_video')

			convert_file(save_path_conv, new_path_conv, render_type, conv_format, start_sec = start_sec,
			                     stop_sec = stop_sec)
			post_content(message, 'json', 'local', new_path_conv, no_caption = False, no_reply = False,
			                caption_str = new_caption, reply_to_message_id = reply_to_message_id)
	except Exception as e:
		bot.send_message(message.chat.id, MSG_ERRORS['EXCEPTION'].format(str(e)), parse_mode = 'HTML')


def pytube_get_videos_list(url_str, subtype='mp4'):
	yt = YouTube(url_str)
	list_of_vids = yt.streams.filter(progressive=True, subtype=subtype).order_by('resolution').desc().all()
	list_of_strs = []
	top_size_limit 		= LIMITS['LOCAL_SIZE_LIMIT'] * 1024 * 1024 * 2
	for i in list_of_vids:
		fsize = i.filesize #bytes
		if fsize > top_size_limit:
			continue
		list_of_strs.append('{0} - {1} - {2}'.format(i.subtype, i.resolution, form_fsize(fsize)))
	return list_of_strs

#
# ---------------- SUPPORT FUNCTIONS
#
def get_convert_options(msg_text):
	conv_format = 'mp4'
	start_sec = None
	stop_sec = None

	src_split = msg_text.split()
	if len(src_split) > 1:
		pattern = re.compile("^([0-9])+(\.)?([0-9])*(\-){1}([0-9])+(\.)?([0-9])*$")
		for i in src_split:
			if i in CONVERT['AVAILABLE_OPTIONS']:
				conv_format = i
			elif pattern.match(i):  # it's range
				list_range = i.split('-')

				start_sec = ast.literal_eval(list_range[0])
				stop_sec = ast.literal_eval(list_range[1])
				if type(start_sec) is not int and type(start_sec) is not float:
					start_sec = None
				if type(stop_sec) is not int and type(stop_sec) is not float:
					stop_sec = None
				if start_sec != None and stop_sec != None and start_sec > stop_sec:
					stop_sec = start_sec

	return [conv_format, start_sec, stop_sec]


def get_render_type(conv_to):
	media_type = 'video'
	render_type = 'to_video'
	if conv_to in MEDIA['TYPES']['VIDEO']:
		render_type = 'to_video'
		doc_types = [
				'webm',
				'avi',
				'3gp',
				'flv',
				'mkv',
				'mpg'
			]
		if conv_to in doc_types:
			media_type = 'doc'
		else:
			media_type = 'video'
	elif conv_to in MEDIA['TYPES']['GIF']:
		render_type = 'to_gif'
		media_type = 'gif'
	elif conv_to in MEDIA['TYPES']['AUDIO']:
		render_type = 'to_audio'
		if conv_to == 'ogg':
			media_type = 'voice'
		else:
			media_type = 'audio'
	return [media_type, render_type]


def convert_type_missmatch(mime_type, render_type):
	ret_val = True
	# GIFS and VIDEOS
	if mime_type in CONVERT['MIME_TYPES_VAG']:
		if render_type == 'to_video' or render_type == 'to_gif' or render_type == 'to_audio':
			ret_val = False
	if mime_type == MEDIA['FORMATS']['AUDIO']['MIME']:
		if render_type == 'to_audio':
			ret_val = False
	return ret_val


def get_save_and_new_path(file_name, conv_to, i_1=None, i_2=None, caption=None):
	new_file_name, new_caption = change_file_ext(file_name, conv_to, i_1 = i_1, i_2 = i_2, caption = caption)
	save_path = '{0}{1}'.format(PATH_TEMP, file_name)
	new_path = '{0}{1}'.format(PATH_TEMP, new_file_name)
	return [save_path, new_path, new_file_name, new_caption]


def change_file_ext(file_name, conv_to, i_1=None, i_2=None, caption=None):
	s_1 = 'start'
	s_2 = 'end'
	new_file_name = file_name

	if i_1 is not None: s_1 = str(i_1)
	if i_2 is not None: s_2 = str(i_2)
	if caption is not None: s_c = caption

	file_name_list = file_name.rsplit('.', 1)
	if len(file_name_list) == 2:
		# we got extension
		file_name_p1 = file_name_list[0]
		file_name_p2 = file_name_list[-1]
		if file_name_p2.lower() == conv_to:
			new_file_name = '{0}_({1}-{2}).{3}'.format(file_name_p1, s_1, s_2, conv_to)
		else:
			new_file_name = '{0}_({1}-{2}).{3}'.format(file_name, s_1, s_2, conv_to)
	else:
		new_file_name = '{0}_({1}-{2}).{3}'.format(file_name, s_1, s_2, conv_to)
	# s_c = '{0}({1}-{2})'.format(s_c, s_1, s_2)
	return [new_file_name, new_file_name]