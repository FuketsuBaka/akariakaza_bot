import re, urllib
from modules.messages import MSG_GOOGLE
from settings.modules_settings import GOOGLE
from googleapiclient.discovery import build


def google(bot, message):
    def _main():
        rawtext = message.text
        rawtext = re.sub(MSG_GOOGLE['RE_1S'], '', rawtext)
        rawtext = re.sub(MSG_GOOGLE['RE_2S'], '', rawtext)
        rawtext = re.sub(MSG_GOOGLE['RE_3S'], '', rawtext)
        query_on_reply = False

        if message.reply_to_message is not None:
            if message.reply_to_message.text is not None and len(message.reply_to_message.text) > 0:
                rawtext = message.reply_to_message.text
                query_on_reply = True

        query_text = " ".join(rawtext.split())

        if not query_on_reply and len(query_text) < 3:
            bot.reply_to(message, MSG_GOOGLE['ERR_SHORT_QUERY'], parse_mode='HTML')
            return

        results = _google_search(query_text, num=3)

        # Parsing results

        if results is None or len(results) == 0:
            bot.reply_to(message, MSG_GOOGLE['ERR_NO_RESULT'], parse_mode='HTML')
            return

        # compose reply
        reply_text = ""
        i = 1
        for result in results:
            result_url = urllib.parse.unquote(result['link'])
            reply_text = reply_text + "{0}: {1}".format(i, result_url) + "\n"
            if i == 1:
                reply_text = reply_text + "<code>-- More links --</code>\n"
            i += 1

        bot.reply_to(message, reply_text, parse_mode='HTML')

    # Google CSE Body
    # Build a service object for interacting with the API. Visit
    # the Google APIs Console <http://code.google.com/apis/console>
    # to get an API key for your own application.
    def _google_search(search_query, **kwargs):
        service = build("customsearch", "v1", developerKey=GOOGLE['SEARCH_API'])
        res = service.cse().list(
            q=search_query,
            cx=GOOGLE['CSE_ID'],
            **kwargs
        ).execute()
        if res['searchInformation']['totalResults'] == '0':
            return None
        return res['items']

    _main()
