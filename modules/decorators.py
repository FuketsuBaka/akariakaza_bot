from datetime import datetime
from threading import Thread, Timer
from functools import wraps
from settings.settings import BOT_USERNAME
from modules.messages import MSG_ERRORS
from modules.blacklists import check_blacklisted

import database.engine as SQL
from modules.utils import get_time_passed
import store

import logging

log = logging.getLogger("Log")

user_spam = {'user_id': {'time': None, 'count': 0}}


def try_except(bot):
    def real_decorator(func):
        def wrapper(message):
            try:
                func(message)
            except Exception as error:
                if log is not None:
                    log.error('Error on func: {0}'.format(error))
                print('Error on func: {0}'.format(error))
                bot.reply_to(message, MSG_ERRORS['EXCEPTION'].format(str(error)), parse_mode='HTML')

        return wrapper

    return real_decorator


def filter_self(func):
    def wrapper(message):
        if message.text and message.text != '':
            if '@' in message.text:
                if BOT_USERNAME not in message.text:
                    return None
        func(message)

    return wrapper


def log_to_db(func):
    def wrapper(message):
        def _log_this(message):
            try:
                SQL.insert_string_log(message)
                SQL.commit_session()
            except Exception as e:
                if str(e).find(1062) != -1:
                    # print('haruka did this')
                    # Stop logging on this chat
                    store.mod_log_disabled_chats(message.chat.id)
                    pass
                else:
                    if log is not None:
                        log.info('Error on log: {0}'.format(e))
                    print('Error on log: {0}'.format(e))

        if message.chat.id not in store.log_disabled_chats:
            Timer(1.0, _log_this, [message]).start()
        func(message)

    return wrapper


def run_async(func):
    @wraps(func)
    def async_func(*args, **kwargs):
        try:
            func_hl = Thread(target=func, args=args, kwargs=kwargs)
            func_hl.start()
            return func_hl
        except Exception as error:
            str_error = 'Error on async: {0}'.format(error)
            log.error(str_error)
            print(str_error)

    return async_func


def blacklist(func):
    def wrapper(message):
        if message is not None:
            msg = None
            from_user = None
            from_chat = None
            if hasattr(message, 'message'):
                # it's a callback
                if hasattr(message, 'message'):
                    msg = message.message
                    from_user = msg.from_user
                    from_chat = msg.chat
                if hasattr(message, 'from_user'):
                    from_user = message.from_user
            elif hasattr(message, 'from_user') and hasattr(message, 'chat'):
                msg = message
                from_user = msg.from_user
                from_chat = msg.chat
            if msg is None or from_user is None or from_chat is None:
                func(message)
                return wrapper

            blacklisted = check_blacklisted(from_user.id, from_chat.id)
            if blacklisted:
                log.info('>>> is blacklisted at least one: user id - {0}, chat id - {1}'.format(
                    message.from_user.id, message.chat.id))
                return None
            else:
                func(message)
        else:
            func(message)
            return wrapper

    return wrapper


def timeout(timeout_seconds, gvar_name):
    def real_decorator(func):
        def wrapper(*args, **kwargs):
            gvar = getattr(store, gvar_name)
            dict_time_passed = get_time_passed(gvar)
            if dict_time_passed['fullseconds'] > timeout_seconds:
                gvar_func_name = 'mod_{0}'.format(gvar_name)
                gvar_func = getattr(store, gvar_func_name)
                if gvar_func is not None:
                    gvar_func(datetime.now())
                return func(*args, **kwargs)
            else:
                return None

        return wrapper

    return real_decorator


def spam(limit, timeout):
    def real_decorator(func):
        def wrapper(message):
            def fn_get_time_passed(start_time, compare_time):
                time_delta = compare_time - start_time
                return time_delta.seconds

            user_id = message.from_user.id
            chat_id = message.chat.id
            # If We (do not)want only private chats
            if user_id == chat_id:
                return func(message)
            msg_time = datetime.utcfromtimestamp(message.date)
            # first message from that User
            if user_id not in user_spam:
                user_spam[user_id] = {'time': msg_time, 'count': 1}
                print(user_id, ' - registered for the first time')
            else:
                # not first message. Check last time
                time_passed = fn_get_time_passed(user_spam[user_id]['time'], msg_time)
                print(user_id, ' - got message after {0} seconds since first'.format(time_passed))

                # timeout is over. Handle as a first message
                if time_passed > timeout:
                    user_spam[user_id] = {'time': msg_time, 'count': 1}
                    print(user_id, ' - timeout is over, count is now = 1')
                # timeout is not over yet. Increase count
                else:
                    user_spam[user_id]['count'] += 1
                    print(user_id, ' - increasing count. It is now = {0}'.format(user_spam[user_id]['count']))
                    if user_spam[user_id]['count'] >= limit:
                        # restrict here
                        print(user_id, ' - !!! is restricted')
            return func(message)

        return wrapper

    return real_decorator
