import socket, re
import store
from modules.utils import sort_dict
from settings.modules_settings import GAMES


def games(bot, message):
    def _get_servers_status(game_type, str_bytes, encoding, bytes, addr):
        def _game_data_transform(rawdata, encoding, game_type):
            rawdata = rawdata.decode(encoding)
            if game_type == 'q3':
                top_data = rawdata.split('\n')
                top_data.pop(0)
                # 0 - status, 1: - players
                server_info = top_data[0].split('\\')
                server_info.pop(0)
                # '0 90 "JefferXXX"'
                players_info_list = top_data[1:]
                players_info_list.pop(-1)
                players_info = {}
                for x in players_info_list:
                    player_stat = x.split()
                    player_frags = int(player_stat[0])
                    player_name = player_stat[-1]
                    player_name = player_name.replace('"', '')
                    player_name = re.sub(r'(\^\d+)', '', player_name)
                    players_info[player_name] = player_frags
                players_info = sort_dict(players_info, True)
                numplayers = len(players_info)

                ser_dict = dict(zip(server_info[::2], server_info[1::2]))
                ser_dict['numplayers'] = numplayers
                ser_dict['player_list'] = players_info
                return ser_dict
            if game_type == 'cs':
                servername, mapname, game, fullname, rest = rawdata[6:-1].split('\0', 4)
                numplayers = ord(rest[2])
                maxplayers = ord(rest[3])
                ser_dict = {'servername': 'unknown',
                            'game': 'unknown',
                            'fullname': 'unknown',
                            'mapname': 'none',
                            'numplayers': '-',
                            'maxplayers': '-'}
                ser_dict['servername'] = servername
                ser_dict['game'] = game
                ser_dict['fullname'] = fullname
                ser_dict['mapname'] = mapname
                ser_dict['numplayers'] = numplayers
                ser_dict['maxplayers'] = maxplayers
                return ser_dict
            return None

        # ------------------------------ _get_servers_status
        is_up = True
        data_result = None
        buff = str_bytes.encode(encoding)
        rawdata = None
        try:
            sock.sendto(buff, addr)
            rawdata, addr = sock.recvfrom(bytes)  # bytes
        except Exception as e:
            is_up = False
        if not rawdata:
            is_up = False
        else:
            data_result = _game_data_transform(rawdata, encoding, game_type)

        return is_up, data_result

    def _parse_game_data(data, is_up, game_type):
        def _parse_player_list(player_list):
            str_players = ''
            for x, y in player_list:
                str_players += '\n<code>' + '%+15s - ' % y + '</code>{0}'.format(x)
            return str_players

        val_status = 'Offline'
        val_players_now = '-'
        val_players_max = '-'
        val_current_map = 'none'
        str_player_list = ''
        if is_up:
            val_status = 'Online'
            if game_type == 'q3':
                val_status = '<b>Online</b> ({0})'.format(data['sv_hostname'])
                val_players_now = data['numplayers']
                val_players_max = data['sv_maxclients']
                val_current_map = data['mapname']
                if len(data['player_list']) > 0:
                    str_player_list = _parse_player_list(data['player_list'])
            if game_type == 'cs':
                val_status = '<b>Online</b> ({0})'.format(data['servername'])
                val_players_now = data['numplayers']
                val_players_max = data['maxplayers']
                val_current_map = data['mapname']
        return_str = ('<code>' + '%+13s' % 'Server is: ' + '</code>{0}\n<code>'.format(val_status)
                      + '%+13s' % 'Map: ' + '</code>{0}\n<code>'.format(val_current_map)
                      + '%+13s' % 'Players: ' + '</code>{0} / {1}'.format(val_players_now, val_players_max))
        if str_player_list != '':
            return_str += str_player_list
        return return_str

    # ------------------------------ mod_games
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(3.0)
    # [is_up, data]
    G_STATUS = {'q3': [False, None],
                'cs': [False, None]}
    # test servers
    # G_STATUS['q3'] = _get_servers_status('q3', MSG_Q3, 'iso-8859-1', ('190.224.16.86', 27962))
    # G_STATUS['cs'] = _get_servers_status('cs', MSG_CS, 'iso-8859-1', 4096, ('193.19.118.230', 27015))

    G_STATUS['q3'] = _get_servers_status('q3', GAMES['MSG_Q3'], 'iso-8859-1', 1500, (store.IP_VG, GAMES['PORT_Q3']))
    G_STATUS['cs'] = _get_servers_status('cs', GAMES['MSG_CS'], 'iso-8859-1', 4096, (store.IP_VG, GAMES['PORT_CS']))

    sock.close()

    report_str = 'HOST:<code> {0} | {1}</code>\n\n'.format(GAMES['DNS_VG'], store.IP_VG)
    report_str = report_str + '<b>QUAKE 3 ARENA</b> [Port: {0}]\n'.format(GAMES['PORT_Q3']) + _parse_game_data(
        G_STATUS['q3'][1],
        G_STATUS['q3'][0],
        'q3') + '\n\n'
    report_str = report_str + '<b>COUNTER STRIKE 1.6</b> [Port: {0}]\n'.format(GAMES['PORT_CS']) + _parse_game_data(
        G_STATUS['cs'][1], G_STATUS['cs'][0], 'cs') + '\n\n'
    report_str = report_str + 'Webpage: {0}'.format(GAMES['VG_ONLINE_MONITOR'])

    bot.send_message(message.chat.id, report_str, parse_mode='HTML')
