import hashlib
import os
import urllib
from datetime import datetime

from htmldom import htmldom

from modules import blacklists as bl
from modules.messages import MSG_SYS
from settings.settings import SU_ID, BOT_ID, PATH_TEMP


def form_fsize(m_size):
    m_size = float(m_size)
    m_size_suffix = MSG_SYS['BB']
    if m_size > 1024:
        m_size = float(m_size / 1024)
        m_size_suffix = MSG_SYS['KB']
        if m_size > 1024:
            m_size = float(m_size / 1024)
            m_size_suffix = MSG_SYS['MB']
            if m_size > 1024:
                m_size = float(m_size / 1024)
                m_size_suffix = MSG_SYS['GB']
    m_size = '{0}{1}'.format('%.2f' % m_size, m_size_suffix)
    return m_size


def fill_dict(message, dict_info, dict_type, **kwargs):
    if dict_type == 'info':
        media = None
        if message.photo is not None:  # is a photo
            media = message.photo[-1]
            dict_info['type']['media_type'] = 'photo'
        if message.audio is not None:  # is an audio
            media = message.audio
            dict_info['type']['media_type'] = 'audio'
        if message.document is not None:  # is a document
            media = message.document
            dict_info['type']['media_type'] = 'document'
            dict_info['type']['is_file'] = 1
        if message.video is not None:  # is a video
            media = message.video
            dict_info['type']['media_type'] = 'video'
        if message.video_note is not None:  # is a videonote
            media = message.video_note
            dict_info['type']['media_type'] = 'video'
        if message.voice is not None:  # is a voice
            media = message.voice
            dict_info['type']['media_type'] = 'voice'
        if message.sticker is not None:  # is a sticker
            media = message.sticker
            dict_info['type']['media_type'] = 'sticker'
            dict_info['type']['is_sticker'] = 1
        if hasattr(media, 'caption'):
            dict_info['vals']['caption'] = media.caption
        if hasattr(media, 'file_id'):
            dict_info['vals']['file_id'] = media.file_id
        if hasattr(media, 'file_name'):
            dict_info['vals']['file_name'] = media.file_name
        if hasattr(media, 'width'):
            dict_info['vals']['width'] = media.width
        if hasattr(media, 'height'):
            dict_info['vals']['height'] = media.height
        if hasattr(media, 'duration'):
            dict_info['vals']['duration'] = media.duration
        if hasattr(media, 'performer'):
            dict_info['vals']['performer'] = media.performer
        if hasattr(media, 'title'):
            dict_info['vals']['title'] = media.title
        if hasattr(media, 'mime_type'):
            dict_info['vals']['mime_type'] = media.mime_type
        if hasattr(media, 'thumb'):
            dict_info['vals']['thumb'] = media.thumb
        if hasattr(media, 'emoji'):
            dict_info['vals']['emoji'] = media.emoji
        if hasattr(media, 'set_name'):
            dict_info['vals']['set_name'] = media.set_name
        if hasattr(media, 'mask_position'):
            dict_info['vals']['mask_position'] = media.mask_position
        if hasattr(media, 'file_size'):
            dict_info['vals']['file_size'] = media.file_size
            if media.file_size is not None:
                dict_info['type']['file_size_f'] = form_fsize(media.file_size)

    if dict_type == 'log':
        obj_user = kwargs.get('obj_user', None)
        obj_chat = kwargs.get('obj_chat', None)
        obj_msg = kwargs.get('obj_msg', None)
        if obj_user is not None and obj_chat is not None and obj_msg is not None:
            dict_info['vals']['user_id'] = obj_user.user_id
            dict_info['vals']['user_name'] = obj_user.username_str
            dict_info['vals']['chat_id'] = obj_chat.chat_id
            dict_info['vals']['chat_type'] = obj_chat.chat_type
            dict_info['vals']['chat_name'] = obj_chat.chat_name
            dict_info['vals']['time_stamp'] = obj_msg.msg_datetime
            dict_info['vals']['msg_id'] = obj_msg.msg_id
            dict_info['vals']['msg_text'] = obj_msg.msg_txt
    return dict_info


def set_dict(dict_type):
    return_dict = None
    if dict_type == 'info':
        return_dict = {'vals': {
            'caption': None,
            'file_id': None,
            'file_size': None,
            'width': None,
            'height': None,
            'duration': None,
            'length': None,
            'mime_type': None,
            'performer': None,
            'title': None,
            'file_name': None,
            'emoji': None,
            'set_name': None,
            'mask_position': None,
            'thumb': None
        }, 'names': {
            'caption': 'Caption',
            'file_id': 'ID',
            'file_size': 'Size',
            'width': 'w',
            'height': 'h',
            'duration': 'Duration',
            'length': 'Length',
            'mime_type': 'MIME',
            'performer': 'Artist',
            'title': 'Title',
            'file_name': 'Name',
            'emoji': 'Emoji',
            'set_name': 'Set',
            'mask_position': 'Pos',
            'thumb': 'Thumb'
        }, 'type': {
            'media_type': None,
            'is_file': 0,
            'is_sticker': 0,
            'file_size_f': ''
        }
    }
    # end if dict_type == 'info'
    if dict_type == 'log':
        return_dict = {'vals': {
            'user_id': None,
            'user_name': None,
            'chat_id': None,
            'chat_type': None,
            'chat_name': None,
            'time_stamp': None,
            'msg_id': None,
            'msg_text': None,
            'msg_info': None,
        }
    }
    # end if dict_type == 'log'
    return return_dict


class Curse(object):
    def __init__(self, curse_str):
        self.source = curse_str

    def show_source(self):
        return self.source

    def is_useless(self):
        return self.source is None or len(self.source) == 0

    def cast(self, list_of_spells):
        for i in self.source:
            spell = i.split()
            spell_words = []
            spell_name = spell[0]
            if len(spell) > 1:
                spell_words = spell[1:]
            if spell_name in list_of_spells:
                return [spell_name, spell_words]
        return None


class User(object):
    def __init__(self, message):
        self.user = message.from_user
        self.user_id = self.user.id
        self.username = self.user.username
        self.username_str = 'Unknown'
        if self.user.last_name is not None:
            self.username_str = '{first_name} {last_name}'.format(
                first_name=self.user.first_name,
                last_name=self.user.last_name
            )
        else:
            self.username_str = self.user.first_name
        self.is_admin = str(self.user_id) == SU_ID
        self.is_this_bot = str(self.user_id) == BOT_ID


class Chat(object):
    def __init__(self, message):
        self.chat = message.chat
        self.chat_id = self.chat.id
        self.chat_type = self.chat.type
        self.chat_name = ''
        if self.chat.username is not None:
            self.chat_name = self.chat.username
        elif self.chat.first_name is not None:
            self.chat_name = self.chat.first_name
            if self.chat.last_name is not None:
                self.chat_name = self.chat_name + ' ' + self.chat.last_name
        elif self.chat.title is not None:
            self.chat_name = self.chat.title


class Msg(object):
    def __init__(self, message):
        self.msg_txt = ''
        self.msg_id = message.message_id
        self.msg_datetime = datetime.utcfromtimestamp(message.date).strftime('%Y-%m-%d %H:%M:%S')
        self.msg_caption = message.caption
        if message.text is not None:
            self.msg_txt = message.text


class Container(User, Chat, Msg):
    def __init__(self, message):
        self.msg = message
        self.obj_user = User(self.msg)
        self.obj_chat = Chat(self.msg)
        self.obj_msg = Msg(self.msg)

        self.dict_media = fill_dict(self.msg, set_dict('info'), 'info')
        self.dict_log = fill_dict(
            self.msg, set_dict('log'), 'log', obj_user=self.obj_user,
            obj_chat=self.obj_chat, obj_msg=self.obj_msg)

        self.dict_log['vals']['msg_info'] = self.info_media()
        if self.dict_log['vals']['msg_text'] == '' and self.dict_media['type']['media_type'] is not None:
            self.dict_log['vals']['msg_text'] = 'Media type: %(m_type)s\nCaption: %(caption)s' % {
                'm_type': self.dict_media['type']['media_type'], 'caption': self.obj_msg.msg_caption}

    # self.dict_log['vals']['msg_info'] = self.dict_log['vals']['msg_info'].replace('<code>', '')
    # self.dict_log['vals']['msg_info'] = self.dict_log['vals']['msg_info'].replace('</code>', '')

    def info_chat(self, memberscount, invite_link):
        bl_str = bl.get_bl_status_string(chat_id=self.obj_chat.chat_id)
        return_str = '<code> ----- CHAT INFO ----- </code>\n'
        return_str = return_str + '<code>' + '{:>12}'.format('Title') + '</code> : {0}\n'.format(
            self.obj_chat.chat_name)
        return_str = return_str + '<code>' + '{:>12}'.format('ID') + '</code> : {0}{1}\n'.format(
            self.obj_chat.chat_id, bl_str[1])
        return_str = return_str + '<code>' + '{:>12}'.format('Type') + '</code> : {0}\n'.format(
            self.obj_chat.chat_type)
        return_str = return_str + '<code>' + '{:>12}'.format('Invite link') + '</code> : {0}\n'.format(
            invite_link)
        return_str = return_str + '<code>' + '{:>12}'.format('Members') + '</code> : {0}\n'.format(
            memberscount)
        return_str = return_str + '<code>' + '{:>12}'.format('Description') + '</code> : {0}\n'.format(
            self.obj_chat.chat.description)
        return return_str

    def info_user(self):
        bl_str = bl.get_bl_status_string(user_id=self.obj_user.user_id)
        return_str = '<code> ----- USER INFO ----- </code>\n'
        return_str = return_str + '<code>' + '{:>12}'.format('Name') + '</code> : {0}\n'.format(
            self.obj_user.username_str)
        return_str = return_str + '<code>' + '{:>12}'.format('Username') + '</code> : {0}\n'.format(
            self.obj_user.username)
        return_str = return_str + '<code>' + '{:>12}'.format('ID') + '</code> : {0}{1}\n'.format(
            self.obj_user.user_id, bl_str[0])
        return return_str

    def info_media(self):
        dict_info = self.dict_media
        if dict_info['type']['media_type'] is None:
            return None
        return_str = '<code> ----- MEDIA INFO ----- </code>\n'
        for i, v in dict_info['vals'].items():
            # print i, v, "---", dict_info['names'][i]
            if v is not None:
                if i == 'width':
                    return_str = return_str + '<code>' + '%+10s' % 'W\H : ' + '</code>{0} x {1} px\n'.format(
                        v, dict_info['vals']['height'])
                elif i == 'height' or i == 'thumb':
                    continue
                elif i == 'file_size':
                    return_str = return_str + '<code>' + '%+10s' % '{0} : '.format(
                        dict_info['names'][i]) + '</code>{0}\n'.format(form_fsize(v))
                else:
                    return_str = return_str + '<code>' + '%+10s' % '{0} : '.format(
                        dict_info['names'][i]) + '</code>{0}\n'.format(v)
        if dict_info['vals']['thumb'] is not None:
            i = dict_info['names']['thumb']
            v = dict_info['vals']['thumb']
            return_str = return_str + '<code>' + '%+10s' % '{0} : '.format(i) + '</code>\n'
            return_str = return_str + '<code>' + '%+5s' % '->' + '%+8s' % 'ID : ' + '</code>{0}\n'.format(
                v.file_id)
            return_str = return_str + '<code>' + '%+5s' % '->' + '%+8s' % 'W\H : ' + '</code>{0} x {1} px\n'.format(
                v.width, v.height)
            return_str = return_str + '<code>' + '%+5s' % '->' + '%+8s' % 'Size : ' + '</code>{0}\n'.format(
                form_fsize(v.file_size))
        return return_str


class Html(object):
    def __init__(self):
        pass

    def set_DOME(self, url):
        self.DOME = htmldom.HtmlDom(url).createDom(
            no_head=True, no_scripts=True
        )

    # returns iterable colection
    # filt = str
    def get_items_by_filter(self, filt):
        return self.DOME.find(filt)

    # attr = str
    def get_item_attrib(self, item, attr):
        return item.attr(attr)


class Hash(object):
    def __init__(self):
        pass

    # Check hash of remote file by url
    def check_hash_url(self, url):
        remote = urllib.request.urlopen(url)
        hash = hashlib.md5()
        buf = remote.read(65536)
        while len(buf) > 0:
            hash.update(buf)
            buf = remote.read(65536)
        return hash.hexdigest()

    # Check hash of file in system
    def hashfile(self, path, blocksize=65536):
        with open(path, 'rb') as f:
            hash = hashlib.md5()
            buf = f.read(blocksize)
            while len(buf) > 0:
                hash.update(buf)
                buf = f.read(blocksize)
            return hash.hexdigest()

    # return list with fullpath and fname
    def get_filepath_by_hash(self, hash, path_to_dir=PATH_TEMP):
        file_list = os.listdir(path_to_dir)
        for file_name in file_list:
            path = os.path.join(path_to_dir, file_name)
            file_hash = self.hashfile(path)
            if file_hash == hash:
                return [path, file_name]
        return [None, None]

    # Compare md5
    def find_existed_md5hash(self, url, path_to_dir=PATH_TEMP):
        remote_hash = self.check_hash_url(url)
        # print 'REMOTE: ', remote_hash
        file_list = os.listdir(path_to_dir)
        for file_name in file_list:
            path = os.path.join(path_to_dir, file_name)
            file_hash = self.hashfile(path)
            # print 'Scanning: ', file_hash
            if file_hash == remote_hash:
                # print '!!!FOUND: ', file_hash
                return path
        return None
