import store
from modules.messages import MSG_ERRORS, MSG_REPLIES
from modules.utils import (
    get_time_passed, syscall, log_top_ip,
    get_size_of_folder, get_humanize_size, is_admin)
from modules.classes import Container
from database.engine import get_distinct_chats, get_last_user_names, get_last_chat_names
from settings.settings import PATH_BASE, PATH_TEMP, LIMITS


def ctl(bot, message):
    original_text = message.text
    cmd = original_text.split()
    if len(cmd) == 1:  # single /cmd
        raise Exception(MSG_ERRORS['NEED_ARGS'])
    report_str = ''
    for i in cmd:
        if i == 'status':
            report_str += '<code>--- AKARI UPTIME ---</code>\n<pre>{0}</pre>\n\n'.format(
                get_time_passed(store.akari_start_time)['string_time_delta'])
            report_str += '<code>--- SYSTEM ---</code>\n<pre>{0}</pre>\n'.format(
                syscall('screenfetch -N -n'))
            report_str += '<code>--- SERVICES ---</code>\n<pre>{0}</pre><pre>{1}</pre>\n'.format(
                syscall('systemctl status akari-app', find_str='Active:', prefix='Akari: ', replace='Active: '),
                syscall('systemctl status mysql', find_str='Active:', prefix='MySQL: ', replace='Active: '))
            report_str += '<code>--- DOWNLOADS ---</code>\n<pre>Temp: {0} / {1}</pre>'.format(
                get_size_of_folder(PATH_TEMP, True), get_humanize_size(LIMITS['DOWNLOADS_MAX_SIZE']))
        if i == 'top_auth':
            report_str += '<code>--- AUTH LOG TOP CONNECTIONS ---</code>\n{0}'.format(log_top_ip('auth.log'))
        if i == 'info':
            cl = None
            if message.reply_to_message is not None:
                cl = Container(message.reply_to_message)
            else:
                cl = Container(message)
            report_str += '\n{0}'.format(cl.info_user())

            # Last user names
            report_str += '<code>' + '{:>12}'.format('AKA') + '</code> :\n'
            user_last_names = get_last_user_names(cl.obj_user.user_id, limit=10)
            for user_id, user_name in user_last_names:
                report_str += '<code>' + '{:>3}'.format('> ') + '</code>{0} \n'.format(user_name)

            invite_link = MSG_ERRORS['FAILED']
            chat_members_count = MSG_ERRORS['FAILED']
            try:
                invite_link = bot.export_chat_invite_link(message.chat.id)
            except:
                invite_link = MSG_ERRORS['FAILED']
            try:
                chat_members_count = bot.get_chat_members_count(message.chat.id)
            except:
                chat_members_count = MSG_ERRORS['FAILED']
            report_str += '\n{0}'.format(cl.info_chat(chat_members_count, invite_link))

            # Last chat names
            report_str += '<code>' + '{:>12}'.format('AKA') + '</code> :\n'
            chat_last_names = get_last_chat_names(cl.obj_chat.chat_id, limit=10)
            for chat_id, chat_name in chat_last_names:
                report_str += '<code>' + '{:>3}'.format('> ') + '</code>{0} \n'.format(chat_name)

            if cl.obj_user.is_admin:
                report_str += '\n' + MSG_REPLIES['IS_ADMIN'] + '\n'
            if cl.obj_user.is_this_bot:
                report_str += '\n' + MSG_REPLIES['IS_THIS_BOT'] + '\n'

            del cl
        if i == 'log':
            report_str += '<code>--- LOG (30) ---</code>\n<pre>{0}</pre>\n'.format(
                syscall('tail -n 30 {0}log'.format(PATH_BASE)))
        if i == 'get_chats':
            report_str += '<code>--- CHATS ---</code>\n{0}\n'.format(
                get_chats(bot))
        # Admin only
        if i == '#do':
            if not is_admin(message.from_user):
                raise Exception(MSG_ERRORS['NOT_ADMIN'])
            exec_cmd = original_text.split(i)
            if len(exec_cmd) == 1:  # single /cmd
                raise Exception(MSG_ERRORS['NEED_ARGS'])
            report_str = '<code>--- RESULTS ---</code>\n<pre>{0}</pre>'.format(
                syscall(exec_cmd[-1])
            )
    if report_str != '':
        bot.send_message(message.chat.id, report_str, parse_mode='HTML')
    else:
        raise Exception(MSG_ERRORS['WRONG_ARGS'])


def get_chats(bot):
    query_result = get_distinct_chats()
    print(query_result)
    chat_list = []
    for i in query_result:
        try:
            # os.sleep(0.25)
            chat = bot.get_chat(i[0])
            if chat.type != 'private':  # exclude privates
                try:
                    inv_link = chat.invite_link
                    if inv_link is None:
                        inv_link = bot.export_chat_invite_link(chat.id)
                        if inv_link is None:
                            inv_link = 'no link'
                except Exception as e:
                    if 'not enough rights' in str(e):
                        inv_link = 'no rights'
                    else:
                        inv_link = 'failed'

                count = bot.get_chat_members_count(chat.id)
                chat_list.append([chat, count, inv_link])
        except Exception as e:
            continue
    if len(chat_list) == 0:
        raise Exception('No chats found.')

    report_str = ''
    for n, q in enumerate(chat_list):
        report_str += '<code>' + '{:>4}'.format(n) + '</code>'
        report_str += ' : <code>' + '{:<15}'.format(q[0].id) + ' : {0} [{1}]</code>'.format(q[0].title, q[1])
        if q[2] == 'no link' or q[2] == 'no rights' or q[2] == 'failed':
            report_str += ' {0}\n'.format(q[2])
        else:
            report_str += ' <a href="{link}">{link}</a>\n'.format(link=q[2])
    return report_str
