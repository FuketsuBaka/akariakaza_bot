#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

from settings.modules_settings import HELP

import os


def main(bot, message):
    cmd = message.text.split()
    file_base = 'help'
    file_suffix = ''
    if len(cmd) == 1:  # single help
        pass
    if len(cmd) > 1:
        file_suffix = cmd[1]
    help_root = os.path.join(HELP['PATH'], HELP['FOLDER'])
    file_name = '{0}_{1}.txt'.format(file_base, file_suffix)
    full_file_name = os.path.join(help_root, file_name)
    if not os.path.isfile(full_file_name):
        raise Exception(
            'No manual found for: "{0}"\nUse "/help commands" to view a list of available manuals.'.format(cmd[1]))
    report_str = ''
    with open(full_file_name, encoding='utf-8', newline='') as f:
        read_arr = [row.rstrip() for row in f]
        for row in read_arr:
            report_str = report_str + row + '\n'
    bot.send_message(message.chat.id, report_str, parse_mode='HTML')
