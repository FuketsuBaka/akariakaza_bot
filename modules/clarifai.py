from clarifai import rest
from clarifai.rest import ClarifaiApp
from clarifai.rest import Image as ClImage

from modules.messages import MSG_CLARIFAI
from modules.utils import sort_dict
from modules.classes import Container
from settings.settings import TELEGRAM_BASE_URL, BOT_TOKEN
from settings.modules_settings import CLARIFAI


def clarifai(bot, message):
    # ---------------------------- CLAI MEDIA
    def _deepmind_media(cl_obj):
        file_info = bot.get_file(cl_obj.dict_media['vals']['file_id'])
        return_str = MSG_CLARIFAI['ANS_EMPTY_3']
        if cl_obj.dict_media['type']['media_type'] != 'photo':
            return cl_obj.dict_log['vals']['msg_info']
        request_url = '{0}{1}/{2}'.format(TELEGRAM_BASE_URL, BOT_TOKEN, file_info.file_path)

        app = ClarifaiApp(api_key='{0}'.format(CLARIFAI['TOKEN']))
        model = app.models.get('general-v1.3')
        image = ClImage(request_url)
        result = model.predict([image])
        if result is not None and result['status']['code'] == 10000:
            sorted_data = result['outputs'][-1]['data']['concepts']
            sorted_data = sorted(sorted_data, key=lambda k: k['value'], reverse=True)
            # print(result_data)
            analize_str = ''
            analize_data = {'90': {}, '80': {}, '50': {}}
            max_i = 10
            max_9_10 = 4
            max_8_9 = 3
            max_7_8 = 3
            max_6_7 = 3
            max_5_6 = 3
            for i in sorted_data:
                if max_i == 0:
                    break
                val = i['value']
                if val >= float(0.5):
                    skip = True
                    if val >= float(0.9) and max_9_10 > 0:
                        analize_data['90'].update({i['name']: int(val * 100)})
                        max_9_10 -= 1
                        skip = False
                    elif float(0.8) <= val < float(0.9) and max_8_9 > 0:
                        analize_data['80'].update({i['name']: int(val * 100)})
                        max_8_9 -= 1
                        skip = False
                    elif float(0.7) <= val < float(0.8) and max_7_8 > 0:
                        analize_data['50'].update({i['name']: int(val * 100)})
                        max_7_8 -= 1
                        skip = False
                    elif float(0.6) <= val < float(0.7) and max_6_7 > 0:
                        analize_data['50'].update({i['name']: int(val * 100)})
                        max_6_7 -= 1
                        skip = False
                    elif val < float(0.6) and max_5_6 > 0:
                        analize_data['50'].update({i['name']: int(val * 100)})
                        max_5_6 -= 1
                        skip = False
                    if skip:
                        continue
                    max_i -= 1
            analize_data['90'] = sort_dict(analize_data['90'], True)
            analize_data['80'] = sort_dict(analize_data['80'], True)
            analize_data['50'] = sort_dict(analize_data['50'], True)
            return_str = ''
            for x, y in analize_data['90']:
                str_name = u'{0}'.format(x)
                str_name = '{:>25} : '.format(str_name)
                analize_str += '<code>' + '{0}'.format(str_name) + '</code>{0}%\n'.format(str(y))
            if len(analize_str) > 0:
                return_str += MSG_CLARIFAI['ANS_GUESS_PHOTO_90'] % analize_str
            analize_str = ''
            for x, y in analize_data['80']:
                str_name = u'{0}'.format(x)
                str_name = '{:>25} : '.format(str_name)
                analize_str += '<code>' + '{0}'.format(str_name) + '</code>{0}%\n'.format(str(y))
            if len(analize_str) > 0:
                return_str += MSG_CLARIFAI['ANS_GUESS_PHOTO_80'] % analize_str
            analize_str = ''
            for x, y in analize_data['50']:
                str_name = u'{0}'.format(x)
                str_name = '{:>25} : '.format(str_name)
                analize_str += '<code>' + '{0}'.format(str_name) + '</code>{0}%\n'.format(str(y))
            if len(analize_str) > 0:
                return_str += MSG_CLARIFAI['ANS_GUESS_PHOTO_50'] % analize_str
            return_str += cl_obj.dict_log['vals']['msg_info']
            return return_str
        else:
            return_str += cl_obj.dict_log['vals']['msg_info']
            return return_str

    # ---------------------------- CLAI TEXT
    def _deepmind_text(cl_obj):
        return MSG_CLARIFAI['ANS_ITS_TEXT']

    # ---------------------------- CLAI MAIN
    if message.reply_to_message is None:
        bot.reply_to(message, MSG_CLARIFAI['ANS_EMPTY_2'], parse_mode='HTML')
        return;
    cl_obj = Container(message.reply_to_message)
    if cl_obj.dict_media['type']['media_type'] is None:
        if cl_obj.obj_msg.msg_txt == '':
            bot.reply_to(message, MSG_CLARIFAI['ANS_EMPTY_1'], parse_mode='HTML')
        else:
            return_str = _deepmind_text(cl_obj)
            if return_str != '':
                bot.reply_to(message, return_str, parse_mode='HTML')
    else:
        bot.send_chat_action(message.chat.id, 'typing')
        return_str = _deepmind_media(cl_obj)
        bot.reply_to(message, return_str, parse_mode='HTML')
    del cl_obj
