# Akari Akaza: Telegram bot
### Booru pics, cat pics, media converting, logging messages to db and other features
---
Polling fired with **[pyTelegramBotApi](https://github.com/eternnoir/pyTelegramBotAPI)**.

Working Bot link: **[@AkariAkaza\_bot](https://t.me/AkariAkaza_bot)** [[mirror](https://tglink.ru/AkariAkaza_bot)].

## Options:
All options are stored in **settings** packages.

###settings.py
First off all - **BOT_TOKEN**. Without it bot won't work (obviously)

Also, here you can specify the modules you want to use in **MODULES**

**BOT_ID** and **SU_ID** are optional. Mostly used in **CTL** module.

### env_settings.py

* PATH_BASE - Fullpath to main.py
* PATH_TEMP - Fullpath to directory where bot will store downloaded files.
* LOCAL\_SIZE\_LIMIT - Limit on file download in Mb.
* DOWNLOADS\_MAX\_SIZE - Maximum size of TEMP folder. Bot will delete and recreate folder after this limit is reached.

### database_settings.py
Here you should provide parameters for your DB connection.

Log-DB will be created automaticaly on bot start-up, if provided DB-User will have enough rights.

If **LOG\_DB** in **MODULES** is set to *False*, sql-connection won't be created.


### modules_settings.py
Each module have it's settings in separate Dictionary.

In some modules you will need to provide your own **API\_KEY** or **TOKEN**.

