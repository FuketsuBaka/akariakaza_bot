--- AKARI GIVECAT  ---
<code>/givecat [format]* [category]*</code> - найти случайного котика. Можно указать <b>одну</b>(ОДНУ! Блджад) категорию:
<code>    hats, space, funny, sunglasses, </code>
<code>    boxes, caturday, ties, dream, </code>
<code>    kittens, sinks, clothes</code>
И формат:<code> gif, jpg, png</code> 

<i>* - необязательный аргумент.</>