#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
import re
from telebot import TeleBot
from settings.settings import *
from modules.messages import *
from modules.decorators import *
from modules.utils import prepare_markup_tables

import database.engine as SQL

bot = TeleBot(BOT_TOKEN)

# ----------------------------- DECORATORS


if MODULES['LOG_LOCAL']:
    import logging

    # ****************** LOGGER
    log = logging.getLogger("Log")
    log.setLevel(logging.INFO)
    fh = logging.FileHandler('{path}log'.format(path=PATH_BASE))
    formatter = logging.Formatter('%(asctime)s [%(name)s]%(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    log.addHandler(fh)
    log.info('Logger instance created')

if MODULES['LOG_DB']:
    SQL.init_connection()

if MODULES['HELP']:
    from modules import help


    @bot.message_handler(commands=['help', 'start', ])
    @log_to_db
    @filter_self
    @try_except(bot)
    def cmd_help(message):
        if log is not None:
            log.info('help command called')
        help.main(bot, message)

if MODULES['CTL']:
    from modules.ctl import ctl


    @bot.message_handler(commands=['ctl'])
    @log_to_db
    @blacklist
    @try_except(bot)
    def cmd_ctl(message):
        if log is not None:
            log.info('ctl command called')
        ctl(bot, message)

if MODULES['GIVECAT']:
    from modules.catapi import catapi


    @bot.message_handler(commands=['givecat', ])
    @log_to_db
    @blacklist
    @try_except(bot)
    def send_givecat(message):
        if log is not None:
            log.info('givecat command called')
        catapi(bot, message)

if MODULES['GIVEPIC']:
    from modules.booru import booru


    @bot.message_handler(commands=['givepic', ])
    @log_to_db
    @blacklist
    @try_except(bot)
    def send_givepic(message):
        if log is not None:
            log.info('givepic command called')
        booru(bot, message)

if MODULES['GOOGLE']:
    from modules.google import google


    @bot.message_handler(commands=['g', ])
    @bot.message_handler(regexp=MSG_GOOGLE['RE_1'])
    @bot.message_handler(regexp=MSG_GOOGLE['RE_2'])
    @bot.message_handler(regexp=MSG_GOOGLE['RE_3'])
    @log_to_db
    @blacklist
    @try_except(bot)
    def send_google(message):
        if log is not None:
            log.info('google command called')
        google(bot, message)

if MODULES['CONVERT']:
    from modules.convert import convert, inline_callback


    @bot.message_handler(commands=['convert', ])
    @log_to_db
    @blacklist
    @try_except(bot)
    def send_convert(message):
        if log is not None:
            log.info('convert command called')
        convert(bot, message)


    @bot.callback_query_handler(func=lambda call: True)
    @blacklist
    @try_except(bot)
    def callback_inline(call):
        if log is not None:
            log.info('called "callback_inline"')
        inline_callback(bot, call)

if MODULES['CLARIFAI']:
    from modules.clarifai import clarifai


    @bot.message_handler(commands=['what', ])
    @bot.message_handler(regexp=MSG_CLARIFAI['RE_1'])
    @bot.message_handler(regexp=MSG_CLARIFAI['RE_2'])
    @log_to_db
    @blacklist
    @try_except(bot)
    def send_re_akari_what(message):
        if log is not None:
            log.info('clarifai command called')
        clarifai(bot, message)

if MODULES['GAMES']:
    from modules.games import games


    @bot.message_handler(commands=['games'])
    @log_to_db
    @blacklist
    @try_except(bot)
    def send_games(message):
        if log is not None:
            log.info('games command called')
        games(bot, message)

if MODULES['ROLL']:
    from modules.roll import roll, dice


    @bot.message_handler(commands=['roll', ])
    @log_to_db
    @blacklist
    @try_except(bot)
    def send_roll(message):
        if log is not None:
            log.info('roll command called')
        roll(bot, message)


    @bot.message_handler(commands=['dice', ])
    @log_to_db
    @blacklist
    @try_except(bot)
    def send_dice(message):
        if log is not None:
            log.info('dice command called')
        dice(bot, message)

# ----------------------------- CATCH ALL MSGS
if MODULES['LISTENER']:
    @bot.message_handler(
        func=lambda m: True,
        content_types=['text', 'photo', 'document', 'audio', 'video', 'sticker', 'video_note', 'voice'])
    @log_to_db
    @blacklist
    def send_direct(message):
        # logger.info('message catched')
        if message.document is not None:
            if message.document.mime_type == 'video/webm' and MODULES['WEBM_FETCH'] and MODULES['CONVERT']:
                if log is not None:
                    log.info('>>> parsed found webm content')
                markup = prepare_markup_tables(
                    [MSG_BTNS['YES_JP'], MSG_BTNS['NO_JP']],
                    False, False, 'ask_convert',
                    message_id=message.message_id)
                msg = bot.send_message(
                    message.chat.id, MSG_MPY['REPLIES']['KB_CONVERT'],
                    reply_markup=markup, reply_to_message_id=message.message_id)
        if message.text is not None and message.text != '':
            if MODULES['WEBM_FETCH'] and MODULES['CONVERT']:
                if re.search(MSG_MPY['REGEXP']['LINK_WEBM'], message.text, re.IGNORECASE):
                    # webm in link
                    if log is not None:
                        log.info('>>> parsed found webm link')
                    markup = prepare_markup_tables(
                        [MSG_BTNS['YES_JP'], MSG_BTNS['NO_JP']],
                        False, False, 'ask_convert',
                        message_id=message.message_id)
                    msg = bot.send_message(
                        message.chat.id, MSG_MPY['REPLIES']['KB_CONVERT'],
                        reply_markup=markup, reply_to_message_id=message.message_id)


def bot_start_polling(**kwargs):
    bot.polling(**kwargs)


# ----------------------------- THREAD
if __name__ == '__main__':
    from datetime import datetime
    from sys import version
    import store

    print(version)

    time_now = datetime.now()
    store.mod_akari_start_time(time_now)
    store.mod_akari_last_call_baka(time_now)
    store.mod_akari_last_call_roll(time_now)

    if log is not None:
        log.info('SET akari_start_time: {0}'.format(store.akari_start_time))
    print('SET akari_start_time: : {0}'.format(store.akari_start_time))

    if MODULES['GAMES']:
        from modules.utils import get_host_by_name

        ip_addr = '139.162.149.103'
        try:
            ip_addr = get_host_by_name(GAMES['DNS_VG'])
        except:
            pass
        store.set_ip_vg(ip_addr)
        if log is not None:
            log.info('IP_VG set to: {0}'.format(store.IP_VG))
        print('IP_VG set to: {0}'.format(store.IP_VG))

    print('--------- 準備が完了しました ---------')
    bot_start_polling(none_stop=True, timeout=120)
    print('--------- タスクは終了しました ---------')
