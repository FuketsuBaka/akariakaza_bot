# Storage for global vars
# MISC
akari_last_call_baka = None
akari_last_call_roll = None
akari_start_time = None
IP_VG = '127.0.0.1'
BOORU_LAST_URL = ''


def mod_akari_last_call_baka(new_val):
    global akari_last_call_baka
    akari_last_call_baka = new_val


def mod_akari_last_call_roll(new_val):
    global akari_last_call_roll
    akari_last_call_roll = new_val


def set_booru_last(url):
    global BOORU_LAST_URL
    BOORU_LAST_URL = url
    print('Recieved to save: %s' % url)
    print('Attempted save: %s' % BOORU_LAST_URL)


def set_ip_vg(ip_addr):
    global IP_VG
    IP_VG = ip_addr


def mod_akari_start_time(new_val):
    global akari_start_time
    akari_start_time = new_val


# Prevent logging to DB if many bots commiting in same chat
# fill this dict on DB commit error 1062
# other bot should not have this
# see modules.decorators log_to_db(func)
log_disabled_chats = []


def mod_log_disabled_chats(chat_id):
    global log_disabled_chats
    log_disabled_chats.append(chat_id)
