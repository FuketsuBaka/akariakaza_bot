# MYSQL
# SQL_HOST	    = 'localhost'
SQL_HOST = ''
SQL_DB = ''
SQL_PORT = 3306
SQL_USER = ''
SQL_PASSWORD = ''

DB_PREFIX = 'aka'

DB_LOG = '{prefix}_log'.format(prefix = DB_PREFIX)
DB_CHATS = '{prefix}_chats'.format(prefix = DB_PREFIX)
DB_USERS = '{prefix}_users'.format(prefix = DB_PREFIX)

# TODO
DB_SETTINGS = '{prefix}_settings'.format(prefix = DB_PREFIX)
DB_BLACKLIST = '{prefix}_blacklist'.format(prefix = DB_PREFIX)
DB_ABOUT = '{prefix}_about'.format(prefix = DB_PREFIX)
