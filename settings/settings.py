# Main bot settings
BOT_TOKEN = ''
SU_ID = ''
BOT_ID = ''
BOT_USERNAME = ''
TELEGRAM_BASE_URL = 'https://api.telegram.org/file/bot'
TELEGRAM_API_URL = 'https://api.telegram.org/bot'

MODULES = {
    'LOG_LOCAL': True,
    'LOG_DB': False,
    'CTL': True,
    'HELP': True,
    'GIVECAT': True,
    'GIVEPIC': True,
    'GOOGLE': True,
    'CLARIFAI': True,
    'CONVERT': True,
    'WEBM_FETCH': True,  # Depends on CONVERT, LISTENER
    'GAMES': True,
    'ROLL': True,
    'ABOUT': True,
    'TALK': True,
    'LISTENER': True,
}

from .env_settings import *
from .modules_settings import *
from .database_settings import *
