import os

DEBUG = False
#PATH_BASE = '/opt/venv/akarin/bot/'
#PATH_TEMP = '/opt/venv/akarin/bot/temp/'
PATH_BASE = './'
PATH_TEMP = './'

# For sad Windows users
if os.name == 'nt':
    DEBUG = True
    #PATH_BASE = 'E:\\Dropbox\\WORK\\PYTHON\\Akari\\akariakaza_bot\\'
    #PATH_TEMP = 'E:\\Dropbox\\WORK\\PYTHON\\Akari\\akariakaza_bot\\temp\\'
    PATH_BASE = 'E:\\path\\to\\bot\\folder\\'
    PATH_TEMP = 'E:\\path\\to\\bot\\folder\\temp\\'

LIMITS = {
    'LOCAL_SIZE_LIMIT': 50,
    'DOWNLOADS_MAX_SIZE': 10 * 1024 * 1024 * 1024  # 10 Gb,
}
