from settings.env_settings import PATH_BASE

MEDIA = {
    'TYPES': {
        'VIDEO': ['mp4', 'mpeg', 'mpg', 'avi', 'webm', '3gp', 'flv', 'mkv'],
        'GIF': ['gif'],
        'AUDIO': ['mp3', 'wav', 'ogg'],
        'IMAGE': ['jpeg', 'jpg', 'png', 'tiff', 'bmp'],
        'DOCUMENT': ['gif', 'doc', 'docx', 'csv', 'xls', 'xlsx', 'txt', 'rtf', 'odt', 'odx', 'pdf'],
    },
    'FORMATS': {
        'WEBM': {'MIME': 'video/webm', 'EXT': 'webm'},
        'MP4': {'MIME': 'video/mp4', 'EXT': 'mp4'},
        'MPG': {'MIME': 'video/mpeg', 'EXT': 'mpg'},
        'MKV': {'MIME': 'video/x-matroska', 'EXT': 'mkv'},
        'AVI': {'MIME': 'video/x-msvideo', 'EXT': 'avi'},
        'FLV': {'MIME': 'video/x-flv', 'EXT': 'flv'},
        '3GP': {'MIME': 'video/3gpp', 'EXT': '3gp'},
        'AUDIO': {'MIME': 'audio/mpeg', 'EXT': 'mp3'},
    },
}

HELP = {
    'PATH': PATH_BASE,
    'FOLDER': 'help',
}

CONVERT = {
    'CHECK_HASH': True,
    'AVAILABLE_OPTIONS': ['webm', 'mp4', '3gp', 'gif', 'mp3', 'avi', 'wav', 'ogg', 'flv', 'mkv'],
    'MIME_TYPES_VAG': [
        MEDIA['FORMATS']['WEBM']['MIME'],
        MEDIA['FORMATS']['MP4']['MIME'],
        MEDIA['FORMATS']['MPG']['MIME'],
        MEDIA['FORMATS']['MKV']['MIME'],
        MEDIA['FORMATS']['AVI']['MIME'],
        MEDIA['FORMATS']['FLV']['MIME'],
        MEDIA['FORMATS']['3GP']['MIME'],
    ],
    'MIME_TYPES_EXT': {
        MEDIA['FORMATS']['WEBM']['MIME']: MEDIA['FORMATS']['WEBM']['EXT'],
        MEDIA['FORMATS']['MP4']['MIME']: MEDIA['FORMATS']['MP4']['EXT'],
        MEDIA['FORMATS']['MPG']['MIME']: MEDIA['FORMATS']['MPG']['EXT'],
        MEDIA['FORMATS']['MKV']['MIME']: MEDIA['FORMATS']['MKV']['EXT'],
        MEDIA['FORMATS']['AVI']['MIME']: MEDIA['FORMATS']['AVI']['EXT'],
        MEDIA['FORMATS']['FLV']['MIME']: MEDIA['FORMATS']['FLV']['EXT'],
        MEDIA['FORMATS']['3GP']['MIME']: MEDIA['FORMATS']['3GP']['EXT'],
        MEDIA['FORMATS']['AUDIO']['MIME']: MEDIA['FORMATS']['AUDIO']['EXT'],
    },
}

# Setup Your ClarifAI token here.
# Obtained via https://clarifai.com/
CLARIFAI = {
    'TOKEN': '',
}
# Setup Your CatAPI token here
# Obtained via https://thecatapi.com/
CATAPI = {
    'TOKEN': '',
    'CATEGORIES': ['hats', 'space', 'funny', 'sunglasses', 'boxes', 'caturday', 'ties', 'dream', 'kittens', 'sinks',
                   'clothes'],
    'BASEURL': 'http://thecatapi.com/api/images/get',
}

# Your Google-search credentials
# Read more at https://developers.google.com/custom-search/v1/overview
GOOGLE = {
    'SEARCH_API': '',
    'CSE_ID': '',
}

BOORU = {
    'OPTIONS': {
        'NO_COMIC': True,
        'RATING_SAFE': True,
    },
    'LIST': {'src': [
        'http://gelbooru.com/index.php',
        # 'http://safebooru.org/index.php'
    ], 'pid_limit': [
        19000,
        # 190000
    ], 'h1_empty': [
        'Nobody here but us chickens!'
        # 'Nothing found, try google?'
    ]
    },
    'S': 's=list',
    'P': 'page=post',
    'V': 's=view',
    'A_LAST_PID': 'alt="last page"',
    'TAG_COMIC': '-comic',
    'TAG_SAFE': 'rating%3Asafe',
}

GAMES = {
    'VG_ONLINE_MONITOR': 'http://ai-frame.net/vg/',
    'DNS_VG': 'vg.ai-frame.net',
    'PORT_Q3': 27960,
    'PORT_CS': 27015,
    'MSG_Q3': '\xFF\xFF\xFF\xFFgetstatus\x00',
    'MSG_CS': '\xFF\xFF\xFF\xFFTSource Engine Query\x00',
}
